# test_technique_effios
## Voici les étapes pour lancer le container de lapp
# Utilisez une image Nginx comme base
FROM nginx:latest

# Copiez les fichiers de build dans le répertoire d'hébergement de Nginx
COPY build /usr/share/nginx/html

# Exposez le port 80 pour accéder à l'application
EXPOSE 80

 # Exécutez la commande pour construire l'image Docker.
 docker build -t test-technique-effios .


# Une fois l'image construite, vous pouvez exécuter un conteneur basé sur cette image.
docker run -p 8080:80 test-technique-effios




