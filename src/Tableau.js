import React, { useState } from 'react';

import './App.css';

function Tableau() {
  const [uai, setUai] = useState('');
  const [granularity, setGranularity] = useState('annee');
  const [results, setResults] = useState([]);

  const fetchAndDisplayData = async () => {
    try {
      const response = await fetch(
        `https://data.education.gouv.fr/api/records/1.0/search/?dataset=fr-en-dnma-par-uai-appareils&q=&facet=debutsemaine&facet=uai&refine.debutsemaine=2020&refine.uai=${uai}`
      );
      const data = await response.json();
      setResults(data.records);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  return (
    <div>

   <h3>1/ formulaire qui prend en entrée l’UAI et un critère Granularité “Année” ou “Mois”,
   “Semaine”, agréger les données suivant ce critère et afficher les résultats dans un tableau.</h3>
      <form>
        <label htmlFor="uai">Code UAI:</label>
        <input
          type="text"
          id="uai"
          name="uai"
          value={uai}
          onChange={(e) => setUai(e.target.value)}
          required
        />
        <br />
        <label htmlFor="granularity">Granularité:</label>
        <select
          id="granularity"
          name="granularity"
          value={granularity}
          onChange={(e) => setGranularity(e.target.value)}
        >
          <option value="annee">Année</option>
          <option value="mois">Mois</option>
          <option value="semaine">Semaine</option>
        </select>
        <br />
        <button type="button" onClick={fetchAndDisplayData}>
          Afficher les Résultats
        </button>
      </form>
      <h2>Résultats</h2>
      <table>
        <thead>
          <tr>
            <th>duree_smartphone</th>
            <th>duree_android</th>
          </tr>
        </thead>
        <tbody>
          {results.map((record) => (
            <tr key={record.recordid}>
              <td>{record.fields.duree_smartphone}</td>
              <td>{record.fields.duree_android}</td>
            </tr>
          ))}
        </tbody>
      </table>

      

   
    </div>
  );
}

export default Tableau;

