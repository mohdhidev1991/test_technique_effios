import React, { useState } from 'react';
import Tableau from './Tableau';
import Chart from './Chart';
import './App.css';

function App() {
  

  return (
    <div className="App">

      <Tableau></Tableau>

      <hr></hr>

      <Chart></Chart>
    </div>
  );
}

export default App;

