import React, { useState } from 'react';
import { Line } from 'react-chartjs-2';


  // Message test

  const Chart = () => {
  const [uai, setUAI] = useState('');
  const [year, setYear] = useState('');
  const [chartData, setChartData] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Ici, je dois appeler mon API pour obtenir les données en fonction de l'UAI et de l'année
    

    const data = {
      // Remplacez ces valeurs par les données récupérées de l'API
      months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      tabletVisits: [100, 150, 200, 180, 250, 300, 280, 220, 210, 230, 220, 180],
      smartphoneVisits: [80, 110, 120, 100, 130, 150, 140, 120, 110, 120, 130, 110],
      computerVisits: [200, 250, 300, 280, 350, 400, 380, 320, 310, 330, 320, 280],
    };

    setChartData(data);
  };
  // Message test
  return (
    <div>
      <h3>2/ Formulaire qui prend en entrée l’UAI et l’année et afficher un graphique présentant
      l’évolution par mois du nombre de visites selon les appareils tablette/smartphone/ordinateur.</h3>
      <form onSubmit={handleSubmit}>
        <label>
          UAI:
          <input type="text" value={uai} onChange={(e) => setUAI(e.target.value)} />
        </label>
        <label>
          Year:
          <input type="text" value={year} onChange={(e) => setYear(e.target.value)} />
        </label>
        <button type="submit">Generate Graph</button>
      </form>

      {chartData && (
        <Line
          data={{
            labels: chartData.months,
            datasets: [
              {
                label: 'Tablet Visits',
                data: chartData.tabletVisits,
                fill: false,
                borderColor: 'blue',
              },
              {
                label: 'Smartphone Visits',
                data: chartData.smartphoneVisits,
                fill: false,
                borderColor: 'green',
              },
              {
                label: 'Computer Visits',
                data: chartData.computerVisits,
                fill: false,
                borderColor: 'red',
              },
            ],
          }}
        />
      )}
    </div>
  );
};
// Message test
export default Chart;
