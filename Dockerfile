
#Créez un fichier de configuration Docker qui permet de servir l’application
# Utilisez une image Nginx comme base
FROM nginx:latest

# Copiez les fichiers de build dans le répertoire d'hébergement de Nginx
COPY build /usr/share/nginx/html

# Exposez le port 80 pour accéder à l'application
EXPOSE 80
gi